package br.com.bs.authenticate.services;

import br.com.bs.authenticate.component.MessageSourceService;
import br.com.bs.authenticate.dto.UserDetailsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;

public abstract class BaseService {

    @Autowired
    protected MessageSourceService message;

    protected String getSessionUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            return auth.getName();
        }
        return null;
    }

    protected Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    protected UserDetailsDTO getUserDetailsDTO() {
        UserDetailsDTO userDetailsDTO = ((UserDetailsDTO) this.getAuthentication().getPrincipal());
        if (Objects.nonNull(userDetailsDTO)) {
            return userDetailsDTO;
        }
        return null;
    }
}
