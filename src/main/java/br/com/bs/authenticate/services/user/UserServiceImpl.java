package br.com.bs.authenticate.services.user;

import br.com.bs.authenticate.dto.UserDetailsDTO;
import br.com.bs.authenticate.entities.Account;
import br.com.bs.authenticate.services.BaseService;
import br.com.bs.authenticate.services.account.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static br.com.bs.authenticate.constantes.ServiceConstants.PREFIX_ROLE;
import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
public class UserServiceImpl extends BaseService implements UserDetailsService {

    private final AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loginFromUsername(username);
    }

    private UserDetailsDTO loginFromUsername(String username) {
        Account account = this.accountService.findByUsername(username);

        if (nonNull(account)) {
            if (account.getEnable()) {
                return this.setLoginFromUsernameSuccess(account);
            } else {
                return this.setAccountDisable();
            }
        }
        throw new UsernameNotFoundException(super.message.get("AppUserDetailsService.usuario.ou.senha.incorretos"));
    }

    private UserDetailsDTO setLoginFromUsernameSuccess(Account account) {
        UserDetailsDTO userDetailsDTO = UserDetailsDTO.builder().build();
        userDetailsDTO.setUsername(account.getUsername());
        userDetailsDTO.setPassword(account.getPassword());
        userDetailsDTO.setEnabled(account.getEnable());
        userDetailsDTO.setAuthorities(this.buildAuthorities(account));
        return userDetailsDTO;
    }

    private UserDetailsDTO setAccountDisable() {
        UserDetailsDTO userDetailsDTO = UserDetailsDTO.builder().build();
        userDetailsDTO.setEnabled(false);
        return userDetailsDTO;
    }

    private Collection<SimpleGrantedAuthority> buildAuthorities(Account account) {
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        account.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(PREFIX_ROLE.concat(role.getRoleName()))));
        return authorities;
    }

}
