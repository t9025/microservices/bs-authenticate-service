package br.com.bs.authenticate.services.account;

import br.com.bs.authenticate.entities.Account;
import br.com.bs.authenticate.exception.BadRequestException;
import br.com.bs.authenticate.repositories.AccountRepository;
import br.com.bs.authenticate.services.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl extends BaseService implements AccountService {

    private final AccountRepository accountRepository;

    @Override
    public Account findByUsername(String username) {
        return this.accountRepository.findByUsername(username)
                .orElseThrow(() ->
                        new BadRequestException(super.message.get("AccountService.usuario.nao.encontrado")));
    }
}
