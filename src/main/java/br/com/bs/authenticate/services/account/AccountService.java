package br.com.bs.authenticate.services.account;

import br.com.bs.authenticate.entities.Account;

public interface AccountService {
    Account findByUsername(String username);
}
