package br.com.bs.authenticate.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;


@Component
public class MessageSourceService {
    private MessageSourceAccessor accessor;

    @Autowired
    public MessageSourceService(final MessageSource messageSource) {
        this.accessor = new MessageSourceAccessor(messageSource, LocaleContextHolder.getLocale());
    }

    public String get(final FieldError fieldError) {
        return this.accessor.getMessage(fieldError);
    }

    public String get(String key) {
        return this.accessor.getMessage(key);
    }

    public String get(String key, String... args) {
        return this.accessor.getMessage(key, args);
    }
}
