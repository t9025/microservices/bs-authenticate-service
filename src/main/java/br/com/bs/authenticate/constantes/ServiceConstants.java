package br.com.bs.authenticate.constantes;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceConstants {

    public static final String PREFIX_ROLE = "ROLE_";

    public static final String[] EXPOSED_HEADERS = new String[]{
            "Authorization",
            "Content-Disposition",
            "Content-Type"
    };
    public static final String[] ALLOWED_HEADERS = new String[]{
            "Authorization"
    };
    public static final String ALLOWED_ORIGINS = "*";
    public static final String[] PUBLIC_MATCHERS = new String[]{
            "/oauth/token",
            "/oauth/token_key",
            "/oauth/check_token",
            "/user"
    };

}
