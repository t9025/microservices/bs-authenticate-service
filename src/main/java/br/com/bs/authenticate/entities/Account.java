package br.com.bs.authenticate.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "acc_account")
public class Account {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "acc_id", nullable = false)
    private Integer id;

    @Column(name = "acc_username", nullable = false)
    private String username;

    @Column(name = "acc_password", nullable = false)
    private String password;

    @Column(name = "acc_first_name", nullable = false)
    private String firstName;

    @Column(name = "acc_last_name", nullable = false)
    private String lastName;

    @Column(name = "acc_enable")
    private Boolean enable;

    @Column(name = "acc_created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "acc_created_by", nullable = false)
    private String createdBy;

    @Column(name = "acc_modified_at")
    private LocalDateTime modifiedAt;

    @Column(name = "acc_modified_by")
    private String modifiedBy;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "acr_account_roles",
            joinColumns = {@JoinColumn(name = "acc_id")},
            inverseJoinColumns = {@JoinColumn(name = "acr_id")})
    private List<AccountRoles> roles;

    @PrePersist
    private void prePersist() {
        this.setCreatedAt(LocalDateTime.now());
    }

    @PreUpdate
    private void perUpdate() {
        this.setModifiedAt(LocalDateTime.now());
    }

}
