package br.com.bs.authenticate.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@ResponseStatus(NO_CONTENT)
public class NoContentException extends RuntimeException {

    public NoContentException(String exception) {
        super(exception);
    }

}
